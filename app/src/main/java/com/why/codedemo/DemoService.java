package com.why.codedemo;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;

/**
 * Created by haoyang on 8/7/2015.
 */
public class DemoService extends Service {
    private final IBinder mBinder = new MyBinder();
    private static PendingIntent pi = null;
    final public static String INTENT_ACTION_UPDATE = "cputemp_update";
    static int upt_interval = 0;

    public DemoService() {
        super();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_NOT_STICKY;
    }
    @Override
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }
    public class MyBinder extends Binder {
        DemoService getService() {
            return DemoService.this;
        }
    }
    public void test() {
        Toast.makeText(this, "Service is running! Working great :)", Toast.LENGTH_SHORT).show();
    }
}
