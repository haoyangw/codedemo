package com.why.codedemo;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
    static int upt_interval = 0;
    final public static String INTENT_ACTION_UPDATE = "cputemp_update";
    private static PendingIntent pi = null;
    private DemoService demo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    @Override
    protected void onResume() {
        super.onResume();
        Intent bind = new Intent(this, DemoService.class);
        bindService(bind, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(mConnection);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder binder)
        {
            DemoService.MyBinder bind = (DemoService.MyBinder)binder;
            demo = bind.getService();
            Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT).show();
        }
        public void onServiceDisconnected(ComponentName className) {
            demo = null;
        }
    };
    public void onClickNoti(View view) {
        int NotID = 51;
        NotificationManager mNotMan = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mNotiTest = new NotificationCompat.Builder(this)
                .setContentTitle("Test Noti")
                .setContentText("Test Notification")
                .setSmallIcon(R.mipmap.ic_launcher);
        mNotMan.notify(NotID, mNotiTest.build());
    }
    public void onClickTestService(View view) {
        if(demo != null) {
            demo.test();
        }
    }
    public static void setAlarm(Context context, int interval) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(INTENT_ACTION_UPDATE);
        pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        if(android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            am.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), interval, pi);
        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            upt_interval = interval;
            am.setExact(AlarmManager.RTC, System.currentTimeMillis() + interval, pi);
        }
    }

    public static void cancelAlarm(Context context, boolean isCustom) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if(pi!=null)  {
            am.cancel(pi);
            pi.cancel();
        }
        if(isCustom) {
            Toast.makeText(context, "CPU Temperature Update cancelled", Toast.LENGTH_SHORT).show();
        }
    }
    /*private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        private boolean isScreenOn = true;

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                isScreenOn = true;
            }
            else if(intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                isScreenOn = false;
            }
            else if(intent.getAction().equals(INTENT_ACTION_UPDATE) && isScreenOn) {
                uptCpu(context);
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    Intent upt = new Intent(INTENT_ACTION_UPDATE);
                    pi = PendingIntent.getBroadcast(context, 0, upt, 0);
                    am.setExact(AlarmManager.RTC, System.currentTimeMillis() + upt_interval, pi);
                }
            }*/
            /*else if(intent.getAction().equals(SettingsActivity.ACTION_SETTINGS_UPDATE)) {
                LXTarget.receivePreferences(context.getSharedPreferences(PREF_KEY, 0), intent);
            }*/
        //}
    //};
}
